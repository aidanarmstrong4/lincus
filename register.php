<?php
/**
 * Created by PhpStorm.
 * User: Aidan
 * Date: 29/05/2018
 * Time: 09:36
*/
?>
<!DOCTYPE html>
<html>
<head>
    <title>Update Guides</title>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/stylesheet.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-light navbar-light">

    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="./index.php">
                <img  src="./icons/lincus-logo.png">
            </a>
        </div>
</nav>

<section>
    <div class="container" style="width: 300px;">
       <h3>Register</h3>
        <form method="post" action="api/register.php">
            <?php

            ?><br>
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter your name">
            <label>username</label>
            <input type="text" name="username" class="form-control" placeholder="Enter a username"><br>
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Enter a password">
           <label>username</label>
           <input type="password" name="password2" class="form-control" placeholder="Re-enter password"><br>
            <input type="submit" class="btn bg-primary" name="submit" value="Register">
        </form>
    </div>
</section>

</body>
</html>