<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 05/06/2018
 * Time: 16:56
 */

session_start();
include 'api/check.php';


$fileContents = file_get_contents("api/types/end-user.json"); // Uses the user type to locate the JSON file contents
$jsonData = json_decode($fileContents, true); // Used to read the JSON data

$allDirectoryFiles = (scandir("api/types"));
$files = array_diff($allDirectoryFiles, array('.', '..'));
?>
<?php include "main-include/login-navbar.php"?>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <select class="form-control" onchange="openModule(this);" id="moduleSelected">
                <option selected>Click to view Modules</option>
                <?php
                $menuCounter = 1; // Counter used to differentiate between user guides
                if (sizeof($files) > 0) {
                    // Loops through each guide
                    foreach ($files as $file) {
                        $fileName = str_replace('-', ' ', $file);
                        $fileName = ucwords(str_replace('.json', ' ', $fileName));
                        echo "<option value=$file> " . $fileName . "</option>"; // Prints the title of the guide in the select
                        $menuCounter++;
                    }
                }
                ?>
            </select>
        </div>
    </div>

    <div class="col-md-12">
        <div class="guides-content">


        </div>
    </div>
</div>
</body>

<script>
    function openModule(file) {

        $(".guides-content").replaceWith(
            "<div class='guides-content'><h3>Loading...</h3></div>");
        var value = file.value;
        $.ajax({
            type: "GET",
            url: "api/dashboard.php",
            data: {value},
            dataType: "html",
            success: function (response) {
                $(".guides-content").replaceWith(response);
            }
        });
    }
</script>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>