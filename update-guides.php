<?php
/**
 * Created by PhpStorm.
 * User: Aidan
 * Date: 27/05/2018
 * Time: 16:57
 */
session_start();
include 'api/check.php';

$message = '';
$error = '';

$description = '';

if(isset($_POST['submit'])){
    if(empty($_POST['user_type'])){
        $error = "<label class='text-danger error'>Please select a user type!</label>";
    }
    else if(empty($_POST['title'])){
        $error = "<label class='text-danger error'>Please enter a title!</label>";
    }
    else if(empty($_POST['description'])){
        $error = "<label class='text-danger error'>Please enter a instruction!</label>";
    }
    else if(empty($_POST['video_url'])){
        $error = "<label class='text-danger error'>Please enter a video url!</label>";
    }
    else {


        if(file_exists('api/types/' .  $_POST["user_type"] . ".json")){
            $description = ($_POST['description']);
            $data = file_get_contents('api/types/' . $_POST["user_type"] . ".json");
            $array_data = json_decode($data, true);


            $extra = array(
                "title" => $_POST["title"],
                "steps" => $description,
                "video_url" => $_POST["video_url"]
            );

            $array_data[] = $extra;
            $final_data = json_encode($array_data);
            if(file_put_contents('api/types/' . $_POST["user_type"] . ".json", $final_data)){
                $message = "<label class='text-success success'>New instruction added. </label>";
            }
            else{
                $error = "<label class='text-danger error'> * Failed to add instruction</label>";
            }
        }else{
            $error = "<label class='text-danger error'>* Json file does not exist!</label>";
        }
    }
}


?>
<?php include "main-include/login-navbar.php"?>
    <section>
        <div class="container" >
            <div class="row">

                <div class="col-md-12">
                    <h3>Add a Guide</h3>
                    <form id="addGuide" method="post">
                        <?php
                        if(isset($error)){
                            echo $error;
                        }
                        ?><br>
                        <label>User Type</label>
                        <select class="form-control" name="user_type" id="user_type" onchange="selectUser()" required>
                            <option selected="selected"  value="">Please choose</option>
                            <option value="end-user">End User</option>
                            <option value="support">Support</option>
                            <option value="regional-support">Regional Support</option>
                            <option value="centre-admin">Centre Administrator</option>
                            <option value="centre-manager">Centre Manager</option>
                            <option value="centre-manager-plus">Centre Manager Plus</option>
                            <option value="regional-admin">Regional Admin</option>
                            <option value="regional-admin-plus">Regional Admin Plus</option>
                            <option value="analyst">Analyst</option>
                            <option value="content-admin">Content Admin</option>
                        </select><br>
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" placeholder="eg: Logging into the lincus" required><br>
                        <div class="input_fields_wrap">
                            <button class="btn btn-primary add_field_button form-control">Add More Steps</button><br><br>
                            <label>Step</label>
                            <div><textarea class="form-control" name="description[]" style="max-width: 95%;" placeholder="eg: Go to www.example.com " required></textarea><br></div>
                        </div>
                        <label>Video Url</label>
                        <input type="text" name="video_url" class="form-control" placeholder="eg: https://www.youtube.com/3dfFgEw" required><br>
                        <input type="submit" class="btn btn-primary" name="submit" value="Add" required><br><br>
                        <?php
                        if(isset($message)){
                            echo $message;
                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 15; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><label>Step</label><textarea class="form-control" style="max-width: 95%;" name="description[]"/><br>' +
                    '<button type="button" class="close stepCloseBtn remove_field" aria-label="Close">' +
                    '<span class="glyphicon glyphicon-trash"></span></button></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

</body>
</html>
