<?php

?>
<!DOCTYPE html>
<html>
<head>
    <title>Lincus Guide</title>
</head>
<body>
<?php include 'api/include/navbar.php' ?>
<div class="main getting-started">
    <div class="container">
        <h1> About Lincus</h1>

        <iframe width="560" height="315" style=" margin-bottom: 10px;"
                src="https://www.youtube.com/embed/8PCnIeeBmqA?rel=0" frameborder="0"
                allow="autoplay; encrypted-media" allowfullscreen>
        </iframe>
        <h1> Lincus Training Guides </h1>
        <div class="guide-wrapper">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

            <?php

            printModuleData("end-user");
            printModuleData("support");
            printModuleData("regional-support");
            printModuleData("centre-admin");
            printModuleData("centre-manager");
            printModuleData("centre-manager-plus");
            printModuleData("regional-admin");
            printModuleData("regional-manager");
            printModuleData("regional-manager-plus");
            printModuleData("analyst");
            printModuleData("content-admin");
            printModuleData("organisational-manager");
            ?>

        </div>
    </div>
</div>

<?php

function printModuleData($module){
    $fileContents = file_get_contents("api/types/" . $module . ".json");
    $jsonData = json_decode($fileContents, true);

    $userTypeFormatted = str_replace('-', ' ', $module);
    $title = ucwords($userTypeFormatted);

    echo <<<EOT

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
               href="#collapse$module">
                <a href="$module.php">$title</a>
            </a>
        </h4>
    </div>
    <div id="collapse$module" class="panel-collapse collapse">
        <div class="panel-body">
            <ol>
EOT;
    $counter = 1;
    foreach ($jsonData as $section) {
        echo ' <li><a href="' . $module . '.php#panel_' . $counter . '">' . $section["title"] . '</a></li>';
        $counter++;
    }
    echo <<<EOT
            </ol>
        </div>
    </div>
</div>
EOT;

}

?>
</body>
</html>