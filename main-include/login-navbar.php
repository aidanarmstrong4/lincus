<?php
/**
 * Created by PhpStorm.
 * User: ljmuinterns
 * Date: 06/06/2018
 * Time: 16:43
 */

?>
<head>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Dashboard</title>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/stylesheet.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- font awesome cdn -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"  crossorigin="anonymous">

</head>
<nav class="navbar navbar-expand-sm bg-light navbar-light">

    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="./dashboard.php">
                <img  src="./icons/logobar.png">
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-tn" style="text-align: center; ">
                <a href="./dashboard.php">
                    <i class="fa fa-home" style="font-size: 30px"></i>
                    <span class="title" style="font-size: 14px;">Home</span>
                </a>
            </li>
    </div>
</nav>
