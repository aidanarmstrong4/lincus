


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

To set up the lincus guide website, you will need a to have a local server installed on your pc.
We built this using xampp localhost server.

Follow the 6 steps below.

* Database configuration

1. Open up phpmyadmin in your hosting server.
2. Create a database name called lincus.
3. Go to the sql editor beside structure and paste in the following code:

       -- phpMyAdmin SQL Dump
       -- version 4.7.7
       -- https://www.phpmyadmin.net/
       --
       -- Host: localhost
       -- Generation Time: Jun 08, 2018 at 10:43 AM
       -- Server version: 10.1.30-MariaDB
       -- PHP Version: 7.1.14
       
       SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
       SET AUTOCOMMIT = 0;
       START TRANSACTION;
       SET time_zone = "+00:00";
       
       
       /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
       /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
       /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
       /*!40101 SET NAMES utf8mb4 */;
       
       --
       -- Database: `lincus`
       --
       
       -- --------------------------------------------------------
       
       --
       -- Table structure for table `users`
       --
       
       CREATE TABLE `users` (
         `id` int(11) NOT NULL,
         `unique_id` varchar(8) NOT NULL,
         `name` varchar(100) NOT NULL,
         `username` varchar(100) NOT NULL,
         `encrypted_password` varchar(80) NOT NULL,
         `salt` varchar(10) NOT NULL,
         `created_at` datetime NOT NULL
       ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
       
       --
       -- Dumping data for table `users`
       --
       
       INSERT INTO `users` (`id`, `unique_id`, `name`, `username`, `encrypted_password`, `salt`, `created_at`) VALUES
       (4, '5b1a3837', 'Admin', 'admin', 'xjTzhffKRjAGZMGYKIJJymf3qmYyYzdhZDU2ODQ3', '2c7ad56847', '2018-06-08 09:03:03');
       
       --
       -- Indexes for dumped tables
       --
       
       --
       -- Indexes for table `users`
       --
       ALTER TABLE `users`
         ADD PRIMARY KEY (`id`);
       
       --
       -- AUTO_INCREMENT for dumped tables
       --
       
       --
       -- AUTO_INCREMENT for table `users`
       --
       ALTER TABLE `users`
         MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
       COMMIT;
       
       /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
       /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
       /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

4. A user table will have been created for you.

        Username: admin || Encrypted password: demouser1
   
   * Deployment instructions
   
5. if you are using localhost travel to localhost/lincus/index.php
    else if you are using your own online web server www.yourdomain.com/index.php but to access
    the database you will need to look the "code and files" section of this README for the config for your
    online web address database. 
    
    You will now have access to the lincus guide website. 
6. To login in and add instructions to guides use the link /login.php and use the details in step 4.


### Code and files ###

If opening up the code use any php IDE, this project was built with PhpStorm but eclipse, visual studio code editors 
will also work fine. 

To access the code open up the lincus folder.
You will see the folders: 
    
    api
    css
    feedbackform
    icons
    img
    main-include

api folder:

    include folder - see below
    TCPDF-master folder - this is the open source plugin for the pdf.
    types folder - All of the jason data files.
    check.php: for checking login for dashboard;
    conn.php: connects to config for secure code
    api-dashboard.php - code for edit/dete page on dashboard.
    delete-module.php - function for deleting. 
    edit-module.php - function for editing.
    logout.php - ends admins session and logs them out of dashboard.  
    process.php - checks login details using functions.php file in the include folder.
    register.php - adds user using function.php file in the include foder. 
            
    include folder:
    
        config.php:   This is where you can define your online websever username and password along with database name;
        navbar.php:  for website navbar uses include on the pages.
        functions.php  for login and password and user.php for all of the jason data code 
        to be able to show different user json.
        


css folder:
    
    stylehseet.css - css code for the website;
    

feedbackFrom folder:
    
    feedbackForm.js - vaildation for feedback form.
    feedbackform.php - resposnse to be sent to receiver (eneter email address to $to variable) to receive response.
    
icons - navbar icons.

img - navbar icons also. (can be replaced)

main-include:

    login-navbar.php - dashboard.php nav, view guides.php nav, update guides.php nav
    


### Who do I talk to? ###
* Repo owner aidanarmstrong 
* Other community or team contact 