<?php
/**
 * Created by PhpStorm.
 * User: Aidan
 * Date: 31/05/2018
 * Time: 16:10
 */

session_start();
include 'api/check.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Update Guides</title>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/stylesheet.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-light navbar-light">

        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="./dashboard.php">
                    <img  src="./icons/logobar.png">
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="navbar-tn" style="text-align: center; ">
                    <a href="api/logout.php">
                        <i class="fa fa-sign-out" style="font-size: 30px"></i>
                        <span class="title" style="font-size: 14px;">Logout</span>
                    </a>
                </li>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="dashboard_button_Wrap" onclick="location=href='update-guides.php'">
                    <a class="btn btn-primary dash-btns"><i class="fa fa-align-justify"></i></a>
                    <span>Add instructions</span>
                </div>
            </div>
            <div  class="col-md-6">
                <div class="dashboard_button_Wrap" onclick="location=href='view-guides.php'">
                    <a class="btn btn-primary dash-btns"><i class="fa fa-archive"></i></a>
                    <span>Edit / Delete Instructions</span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>