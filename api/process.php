<?php
session_start();

require_once 'include/functions.php';
$db = new Functions();

// json response array

if (isset($_POST['username']) && isset($_POST['password'])) {
	
	// receiving the post params
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	// get the user by email and password
	$user = $db->getUserByUsernameAndPassword($username, $password);
	
	if ($user != false) {
		//user is found 
		$_SESSION['loggedin'] = true;
		$_SESSION["userid"] = trim($user["id"]);
		$_SESSION["name"] = $user["name"];
		$_SESSION["username"] = $user["username"];
		//start session() and redirect to homepage.
		header("Location: ../dashboard.php");
	} else {
		// user is not found with the credentials
		$_SESSION['Error'] = "<label class='text-danger error'>* Username or Password is Incorrect! </label>";
		header("Location:../login.php");
	}
}

ini_set('display_errors', 1);