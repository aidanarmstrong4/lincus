<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 06/06/2018
 * Time: 14:16
 */

$fileName = $_GET["value"];
$fileContents = file_get_contents("types/" . $fileName); // Gets the file contents
$jsonData = json_decode($fileContents, true); // Used to read the JSON data

require_once('edit-module.php');
?>
<div class="guides-content">
    <div class="row">
        <div class="col-md-12">
            <br><select class="form-control" onchange="navigateToModule(this);" id="moduleSelected">
                <option selected>Click to view Modules</option>
                <?php
                $menuCounter = 1; // Counter used to differentiate between user guides
                if (sizeof($jsonData) > 0) {
                    // Loops through each guide
                    foreach ($jsonData as $module) {
                        $id = "panel_" . $menuCounter;
                        echo "<option value=$id> " . $module["title"] . "</option>"; // Prints the title of the guide in the select
                        $menuCounter++;
                    }
                }
                ?>
            </select>
        </div>
    </div>

    <div class="guide-instructions">
        <div class="panel-group" id="accordion">
            <?php
            $accordionCounter = 1; // Counter used to differentiate between user guides
            if (sizeof($jsonData) > 0) {
                foreach ($jsonData as $module) {
                    ?>
                    <form id="form_<?php echo $accordionCounter ?>" action="api/edit-module.php" method="POST">
                        <div class="panel panel-default" id="panel_<?php echo $accordionCounter ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="btn-block" data-parent="#accordion"
                                       href="#id_<?php echo $accordionCounter ?>">
                                        <?php echo $module["title"] ?></a>
                                    <!-- passing counter & video url to function -->
                                </h4>
                            </div>
                            <div id="id_<?php echo $accordionCounter ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 guide-wrapper">
                                            <div class="form-group">
                                                <span class="pull-right btn btn-danger" onclick="deleteModule('<?php echo $accordionCounter ?>', '<?php echo $fileName?>')">Delete Module</span><br>
                                                <label for="title">Title:</label>
                                                <input type="text" id="title" class="form-control"
                                                       name="title" value="<?php echo $module["title"]; ?>"> <br>
                                            </div>
                                            <div class="form-group">
                                                <label for="video_url">Video URL:</label>
                                                <input type="text" class="form-control"
                                                       name="video_url" value="<?php echo $module["video_url"]; ?>">
                                                <br>
                                            </div>
                                            <?php
                                            $editAccordion = 1;
                                            foreach ($module["steps"] as $step) {
                                                ?>
                                                <div class="form-group" id="step_<?php echo $editAccordion ?>">
                                                    <p>
                                                        <strong>Step:</strong> <?php echo $editAccordion ?>
                                                        <span class="pull-right stepCloseBtnBin"><span
                                                                    class='pull-right glyphicon glyphicon-trash'
                                                                    onclick=removeStep("<?php echo $editAccordion - 1 ?>","<?php echo $accordionCounter - 1 ?>","<?php echo $fileName ?>")></span>
                                                        </span>
                                                        <br>
                                                    </p>
                                                    <textarea style="width: 95%;" name="steps[]"
                                                              class="form-control"><?php echo $step ?></textarea>
                                                </div>
                                                <?php
                                                $editAccordion++;
                                            }
                                            ?>
                                            <input type="hidden" value="<?php echo $fileName ?>" name="file_name">
                                            <input type="hidden" value="<?php echo $accordionCounter - 1 ?>"
                                                   name="index">
                                            <input type="button" class="btn btn-primary add_field_button"
                                                   value="Add Step"
                                                   onclick="addNewStep('<?php echo $accordionCounter ?>', '<?php echo $editAccordion; ?>')">
                                            <input type="submit"
                                                   class="btn btn-success" value="Submit">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <?php
                    $accordionCounter++;
                }
            }
            ?>
        </div>
    </div>
</div>
</div>

<script>
    var stepCounter = 0;

    function navigateToModule(value) {
        //$('#id_' + value.selectedIndex).collapse('toggle');
        location.hash = value.options[value.selectedIndex].value;
    }

    function addNewStep(moduleid, stepid) {
        if(stepCounter == 0){
            stepCounter = stepid;
        } else {
            stepCounter++;
        }

        // Creates a new text area under the last step
        $("#id_"+moduleid+" .form-group:last").after('<div class="form-group" id=step_'+stepCounter+'><label>Step</label><textarea style="width:95%;" class="form-control" name="steps[]"/><br>' +
            '<span class=\'pull-right glyphicon glyphicon-trash\' onclick=removeStepTextbox('+stepCounter+','+moduleid+'); ></span></div>');
        }


    $('.guide-wrapper').on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
    })

    function removeStep(stepid, moduleid, file) {
        if (window.confirm("Are you sure you wish to delete this step?")) {
            $.ajax({
                type: "POST",
                url: "api/delete-module-step.php",
                data: {stepid, moduleid, file},
                dataType: "html",
                success: function () {
                    stepid++;
                    moduleid++;
                    removeStepTextbox(stepid, moduleid);
                }
            });
        };
    }

    function removeStepTextbox(stepid, moduleid) {
        if (stepid != null && moduleid != null) {
            $('#id_' + moduleid + ' #step_' + stepid).remove();
        }
    }

    function deleteModule(moduleid, file){
        moduleid--;
        if(confirm("WARNING: This will delete the module permanently. Confirm below:")){
            $.ajax({
                type: "POST",
                url: "api/delete-module.php",
                data: {moduleid, file},
                dataType: "html",
                success: function (response) {
                    moduleid++;
                    $("#form_"+moduleid).remove();
                }
            });
        }
    }

</script>