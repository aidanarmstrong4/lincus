<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 07/06/2018
 * Time: 12:07
 */

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $stepID = $_POST['stepid'];
    $moduleID = $_POST['moduleid'];
    $fileName = $_POST['file'];


    $fileContents = file_get_contents("types/" . $fileName); // Gets the file contents
    $jsonData = json_decode($fileContents, true); // Used to read the JSON data

    unset($jsonData[$moduleID]["steps"][$stepID]);

    $newValue = array_values($jsonData[$moduleID]["steps"]);

    $jsonData[$moduleID]["steps"] = $newValue;
    $newJsonData = json_encode($jsonData);

    file_put_contents("types/" . $fileName, $newJsonData);
}
