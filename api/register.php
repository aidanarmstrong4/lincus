<?php
session_start();
require_once 'include/functions.php';
$db = new Functions();


if (isset($_POST['name']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2'])) {
	if($_POST['password'] == $_POST['password2']){
	
	// receiving the post params
	$name = $_POST['name'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	$_SESSION['Approved'] = "<label class='text-success success'>Account created! Please Log in!</label>";
	header("Location: ../login.php");
	// check if user is already existed with the same email
	if ($db->isUserExisted($username)) {
		// user already existed
		$_SESSION['Error'] = "<label class='text-danger error'>".$username."  already has an account! </label>";
		header("Location: ../login.php");
	} else {
		// create a new user
		$user = $db->storeUser($name, $username, $password);
		if ($user) {
			// user stored successfully
			echo $user["unique_id"];
			echo $user["name"];
			echo $user["username"];
			echo $user["created_at"];
		} else {
			// user failed to store
			$_SESSION['Error'] = "<label class='text-danger error'>Unknown error occurred in registration!</label>";
			header("Location: ../login.php");
		}
	}
	} else{
		$_SESSION['Error'] = "Password does not match";
		header("Location: ../login.php");
	}
} else {
	$_SESSION['Error'] = "Fields not filled in!";
	header("Location: ../login.php");
}