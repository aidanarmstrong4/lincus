<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 22/05/2018
 * Time: 15:32
 *
 */

?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Disable Pintrest Pin -->
    <meta name="pinterest" content="nopin" description="Sorry, pinterest not available" />


    <!-- Include the Open Graph tags -->
    <meta property="og:title" content="Lincus - Helping people lead healthier lives">
    <meta property="og:site_name" content="Lincus">
    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="en_GB">
    <meta property="og:image" content="/assets/images/lincus_og_image.png">
    <meta property="og:description" content="The Lincus (pronounced 'Link Us') project has been an ongoing mission for Rescon, a UK technology company, since 2011. In a quest to provide an accessible, easy to use and extremely useful communication tool that assists decision-making and integrates care recording. Rescon has worked with multiple partners and subject matter experts to develop Lincus. After four years of development and usability trials we are excited to release Lincus as a commercial offering in 2015. We will be releasing our free Lincus web app in May followed by our free iPhone and Android apps in June. If you would like to find our more about what we are planning for the future, please contact us.">

    <!-- Icons -->
    <link href="icons/logobar.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/stylesheet.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body onload="checkCookie()">
<nav class="navbar navbar-expand-sm bg-light navbar-light">

    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./index.php">
                <img  src="./icons/logobar.png">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="nav navbar-nav">
            <li>
                <a href="./end-user.php">
                    <img src="./icons/enduser.png">
                    <span class="title">End user</span>
                </a>
            </li>
            <li>
                <a href="./support.php">
                    <img src="./img/survey.png">
                    <span class="title">Support</span>
                </a>
            </li>
            <li>
                <a href="./regional-support.php">
                    <img src="./img/survey.png">
                    <span class="title">Regional Support</span>
                </a>
            </li>
            <li>
                <a href="./centre-admin.php">
                    <img src="./img/devices.png">
                    <span class="title">Centre Admin</span>
                </a>
            </li>
            <li>
                <a href="./centre-manager.php">
                    <img src="./img/admin.png">
                    <span class="title">Centre Manager</span>
                </a>
            </li>
            <li>
                <a href="./centre-manager-plus.php">
                    <img src="./img/staff.png">
                    <span class="title">Centre Manager +</span>
                </a>
            </li>
            <li>
                <a href="./regional-admin.php">
                    <img src="./icons/regional.png">
                    <span class="title">Regional Admin</span>
                </a>
            </li>
            <li>
                <a href="./regional-manager.php">
                    <img src="./img/communication.png">
                    <span class="title">Regional Manager</span>
                </a>
            </li>
            <li>
                <a href="./regional-manager-plus.php">
                    <img src="./img/communication.png">
                    <span class="title">Regional Manager +</span>
                </a>
            </li>
            <li>
                <a href="./analyst.php">
                    <img src="./img/alerts.png">
                    <span class="title">Analyst</span>
                </a>
            </li>
            <li>
                <a href="./content-admin.php">
                    <img src="./img/events.png">
                    <span class="title">Content Admin</span>
                </a>
            </li>
            <li>
                <a href="./organisational-manager.php">
                    <img src="./icons/organisational.png">
                    <span class="title">Organisational Manager </span>
                </a>
            </li>
        </ul>
        </div>
    </div>
</nav>

<div class="container footer-button">
    <button type="button" class="btn btn-primary feedback" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-comments"></i></button>
    <a id="scroll-up" href="#" class="btn btn-primary" role="button" title="Click to return on the top page" data-toggle="tooltip" ><span class="glyphicon glyphicon-chevron-up"></span></a>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Feedback message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="feedbackForm" role="form" action="feedbackform/feedbackform.php" method="POST">
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message:</label>
                            <textarea class="form-control"  id="message-text" name="message" rows="5" data-rule="required" data-msg="* Please write something for us!" placeholder="Message"></textarea>
                            <div class="validation text-danger"></div>
                        </div>
                        <div class="form-group end-btns">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Send message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!--.container-->
<div id="cookie_consent"></div>
<script type="text/javascript" src="feedbackform/feedbackform.js"></script>

<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#scroll-up').fadeIn();
            } else {
                $('#scroll-up').fadeOut();
            }
        });
        $('#scroll-up').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    $(document).ready(function(){
        setTimeout(function () {
            $("#cookieConsent").fadeIn(100);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function() {
            $("#cookieConsent").fadeOut(200);
        });
    });

    function setCookie(cookie_name, value, days) {
        console.log("Creating cookie");
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + days);
        document.cookie = cookie_name+ "=" + escape(value)+
            ((days == null) ? "" : ";expires=" + exdate.toGMTString());
        console.log("Cookie Executed");
    }

    function getCookie(cookie_name) {
        var name = cookie_name + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


    function checkCookie() {
        var username = getCookie("cookieConsent");

        if (username != "cookieConsent") {

             document.getElementById("cookie_consent").innerHTML = ("<div class='container top cookie-wrap'>\n" +
            "    <div id='cookieConsent'>\n" +
            "        <div id='closeCookieConsent'>&times;</div>\n" +
            "        This website uses cookies to ensure you get the best experience on our website. <a href='' target='_blank'>Cookie policy</a>.\n" +
            "        <button id='cookieConsentOK' onclick='setCookie("+ cookie_name +",true, 1)' class='btn btn-primary cookieConsentOK'>Accept</button>\n" +
            "    </div>\n" +
            "</div>");


        } else {
            alert("cookie set");
        }
    }

</script>

</body>

</html>
