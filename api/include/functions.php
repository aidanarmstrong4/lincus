<?php

class Functions {
	private $conn;
	
	// constructor
	function __construct() {
		require'conn.php';
		// connecting to database
		$db = new Connect();
		$this->conn = $db->connect();
	}
	
	// destructor
	function __destruct() {
		
	}
	
	/**
	 * Storing new user
	 * returns user details
	 */
	public function storeUser($name, $username, $password) {
		$uuid = uniqid('', true);
		$name = $_POST['name'];
		$hash = $this->hashSSHA($password);
		$encrypted_password = $hash["encrypted"]; // encrypted password
		$salt = $hash["salt"]; // salt
		
		$stmt = $this->conn->prepare("INSERT INTO users(unique_id, name, username, encrypted_password, salt, created_at) VALUES(?, ?, ?, ?, ?, NOW())");
		$stmt->bind_param("sssss", $uuid, $name,  $username, $encrypted_password, $salt);
		$result = $stmt->execute();
		$stmt->close();
		
		// check for successful store
		if ($result) {
			$stmt = $this->conn->prepare("SELECT * FROM users WHERE username = ?");
			$stmt->bind_param("s", $username);
			$stmt->execute();
			$user = $stmt->get_result()->fetch_assoc();
			$stmt->close();
			
			return $user;
		} else {
			return false;
		}
	}
	
	/**
	 * Get user by username and password
	 */
	public function getUserByUsernameAndPassword($username, $password) {



	    $sql = "SELECT id, unique_id, name, username, encrypted_password, salt, created_at FROM users WHERE username = ?";

        if($stmt = $this->conn->prepare($sql)){
            $stmt->bind_param("s", $username);
            $stmt->execute();

            $stmt->store_result();

            $stmt->num_rows;

            $stmt->bind_result($id, $unique_id, $name, $username, $encrypted_password, $salt, $created_at);
            while ($user = $stmt->fetch()) {
                $stmt->close();
                // verifying user password
                $hash = $this->checkhashSSHA($salt, $password);
                // check for password equality
                if ($encrypted_password == $hash) {
                    // user authentication details are correct
                    return $user;
                }
            }

        } else {
            return NULL;
        }
	}
	
	/**
	 * Check user is existed or not
	 */
	public function isUserExisted($username) {
		$stmt = $this->conn->prepare("SELECT username from users WHERE username = ?");
		
		$stmt->bind_param("s", $username);
		
		$stmt->execute();
		
		$stmt->store_result();
		
		if ($stmt->num_rows > 0) {
			// user existed
			$stmt->close();
			return true;
		} else {
			// user not existed
			$stmt->close();
			return false;
		}
	}
	
	/**
	 * Encrypting password
	 * @param password
	 * returns salt and encrypted password
	 */
	public function hashSSHA($password) {
		
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10);
		$encrypted = base64_encode(sha1($password . $salt, true) . $salt);
		$hash = array("salt" => $salt, "encrypted" => $encrypted);
		return $hash;
	}
	
//	/**
//	 * Decrypting password
//	 * @param salt, password
//	 * returns hash string
//	 */
	public function checkhashSSHA($salt, $password) {
		
		$hash = base64_encode(sha1($password . $salt, true) . $salt);
		
		return $hash;
	}
//	public function checkUserLoggedIn($userId){
//
//		if(!isset($_SESSION['user_id'])){ //if login in session is not set
//			$_SESSION['redirectURL'] = $_SERVER['$REQUEST_URI'];
//			header("Location: ../login.php");
//		}
//		$records = $this->conn->prepare("SELECT * from users WHERE id = id");
//// 		$records->bindParam(':id', $_SESSION['user_id']);
//		$records->execute();
//		$results = $records->fetch(PDO::FETCH_ASSOC);
//
//		$user = NULL;
//
//		if(count($results) > 0){
//			$user = $results;
//		}
//
//		return $checkUser;
//
//	}

}