<?php
$link = $_SERVER['PHP_SELF']; // Retrieves the link
$link_array = explode('/', $link);
$userType = end($link_array); // Gets the last section of the link (the user type)
$userType = str_replace('.php', '', $userType);

$fileContents = file_get_contents("api/types/" . $userType . ".json"); // Uses the user type to locate the JSON file contents

$userTypeFormatted = str_replace('-', ' ', $userType);
$userTypeFormatted = ucwords($userTypeFormatted); // Used for the title of the user type
$jsonData = json_decode($fileContents, true); // Used to read the JSON data
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $userTypeFormatted . ' Guide'?></title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
<?php include 'api/include/navbar.php' ?>
<div class="main user">
    <div class="container">
        <form method="post" target="_blank" action="view_pdf.php">
            <input type="hidden" value="<?=http_build_query($jsonData);?>" name="jsonData" />
            <button class="pull-right" type="submit" name="create_pdf" title="View PDF"><i class="fa fa-file-pdf-o" style="font-size: 35px;"></i></button>
        </form><br>
        <h1> Lincus Training Guides - <?php echo $userTypeFormatted; ?></h1>

        <div class="row">
            <div class="col-md-12">
                <select class="form-control" onchange="navigateToModule(this, '<?php echo $userType?>');" id="moduleSelected">
                    <option selected>Click to view Modules</option>
                    <?php
                    $menuCounter = 1; // Counter used to differentiate between user guides
                    if (sizeof($jsonData) > 0) {
                        // Loops through each guide
                        foreach ($jsonData as $module) {
                            $id = "panel_" . $menuCounter;
                            echo "<option value=$id> " . $module["title"] . "</option>"; // Prints the title of the guide in the select
                            $menuCounter++;
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="guide-instructions">
            <div class="panel-group" id="accordion">
                <?php
                $accordionCounter = 1; // Counter used to differentiate between user guides
                if (sizeof($jsonData) > 0) {
                    foreach ($jsonData as $module) {
                        ?>
                        <div class="panel panel-default" id="panel_<?php echo $accordionCounter ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="btn-block" data-parent="#accordion"
                                       href="#id_<?php echo $accordionCounter ?>">
                                        <?php echo $module["title"] ?></a> <!-- passing counter & video url to function -->
                                </h4>
                            </div>
                            <div id="id_<?php echo $accordionCounter ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form method="post" target="_blank" action="view_pdf.php">
                                        <input type="hidden" value="<?=http_build_query([$module]);?>" name="jsonData" />
                                        <button class="pull-right" type="submit" name="create_pdf" title="View PDF"><i class="fa fa-file-pdf-o" style="font-size: 35px;"></i></button>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <div id="video-holder">

                                            </div>
                                            <?php if(($module["video_url"]) != ""): ?>
                                                <button class="btn btn-success" value="Load PDF" onclick="loadFrame(<?php echo $accordionCounter; ?>,'<?php echo $module["video_url"]; ?>', this);">Load Video</button>
                                            <?php endif; ?>
                                            <br>
                                        </div>
                                    </div>
                                    <ol>
                                        <?php foreach ($module["steps"] as $step) {
                                            echo "<li>" . $step . "</li>";
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <?php
                        $accordionCounter++;
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 500);
        });
    });

    function navigateToModule(value, file) {
        $("#id_" + value.selectedIndex).collapse('toggle');
        location.hash = value.options[value.selectedIndex].value;
        var selected = value.selectedIndex;
        selected--;
        $.ajax({
            type: "GET",
            url: "api/view_module_pdf.php",
            data: {selected, file},
            dataType: "html",
            success: function (response) {

            }
        });
    }

    function loadFrame(id, video_url, button) {
        var frameId = "frame_" + id;
        $('#id_' + id + ' #video-holder').html("<div id=\"video-holder\">\n" +
            "<iframe id='" + frameId + "' width=\"560\" height=\"315\"\n" +
            "frameborder=\"0\" src='" + video_url +"'\n" +
            "allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>\n" +
            "</div>");

        button.remove();
    }

</script>