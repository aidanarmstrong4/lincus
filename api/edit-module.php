<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 07/06/2018
 * Time: 10:00
 */

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $title = $_POST["title"];
    $video_url = $_POST["video_url"];
    $steps = $_POST["steps"];
    $fileName = $_POST["file_name"];
    $index = $_POST["index"];

    $fileContents = file_get_contents("types/" . $fileName); // Gets the file contents
    $jsonData = json_decode($fileContents, true); // Used to read the JSON data

    $jsonData[$index]["title"] = $title;
    $jsonData[$index]["video_url"] = $video_url;
    $jsonData[$index]["steps"] = $steps;

    $newJsonData = json_encode($jsonData);
    file_put_contents("types/" . $fileName, $newJsonData);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
}