<?php
/**
 * Created by PhpStorm.
 * User: rinch
 * Date: 08/06/2018
 * Time: 12:38
 */

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $moduleid = $_POST["moduleid"];
    $fileName = $_POST["file"];

    $fileContents = file_get_contents("types/" . $fileName); // Gets the file contents
    $jsonData = json_decode($fileContents, true); // Used to read the JSON data

    unset($jsonData[$moduleid]);

    $newJsonData = json_encode($jsonData);
    file_put_contents("types/" . $fileName, $newJsonData);
}