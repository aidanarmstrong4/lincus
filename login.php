<?php
/**
 * Created by PhpStorm.
 * User: Aidan
 * Date: 29/05/2018
 * Time: 09:03
 */

session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Dashboard</title>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/stylesheet.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- font awesome cdn -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"  crossorigin="anonymous">

</head>

<body style="background-color: #edf1f9)">
<nav class="navbar navbar-expand-sm bg-light navbar-light">

<div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="./index.php">
            <img  src="./icons/logobar.png">
        </a>
    </div>
</div>
</nav>
<section>
    <div class="container  " style="width: 350px;">
        <div class="login-title">
            <h3>Login <i class="fa fa-sign-in-alt"></i></h3>
        </div><br>
        <form class="loginWrap" method="post" action="api/process.php">
            <br>
            <label>Username</label>
            <input type="text" name="username" class="form-control" placeholder="example123"><br>
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password"><br>
            <?php if(isset($_SESSION['Error'])):?>
                <?php
                echo $_SESSION ['Error'];
                unset ( $_SESSION ['Error'] );
                session_destroy (); ?><br>
            <?php endif;?>
            <button type="submit" style="width: 100%;" class="btn btn-success" name="submit">Log in</button><br><br>

            <?php if(isset($_SESSION['Approved'])):?>
                <?php
                    echo $_SESSION ['Approved'];
                    unset ( $_SESSION ['Approved'] );
                    session_destroy ();
                    ?>
            <?php endif;?>
        </form>
    </div>
</section>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $(".error").effect("shake", {times: 2}, 400 );
    });
</script>
</body>
</html>
